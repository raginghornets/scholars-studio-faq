#+title: Scholars Studio FAQ
#+author: Eric Nguyen
#+export_file_name: index
#+setupfile: https://fniessen.github.io/org-html-themes/setup/theme-readtheorg.setup
#+html_head: <link rel="icon" type="image/png" href="./favicon.png"/>
#+html_head: <style>#table-of-contents h2{background: rgb(157, 34, 53);}</style>
#+html_head: <style>#postamble .date{color: rgb(138, 141, 143);}</style>
* What is this space?
This is the Loretta C. Duckworth Scholars Studio.

#+begin_quote
Temple Libraries’ Scholars Studio serves as a space for student and faculty consultations, workshops, and collaborative research in digital humanities, digital arts, cultural analytics, and critical making.

We offer a wide-range of technical equipment, software, and support for scholarly practices involving digital methods for interdisciplinary research and pedagogy, including text mining and analysis, working in and creating 3D spaces, using geospatial technology, incorporating games into education, and much more.
#+end_quote

For more information, visit our official website [[https://sites.temple.edu/tudsc/][here]].
* Where is room ...?
If you want, you can take one of the maps we have here.
They aren't very detailed, but maybe they could help guide you.
** 310
This entire space, the Scholars Studio, is room 310.
For whatever reason, the planners of the library decided to put room numbers to every space, even if it's not a room.
** 311, 312
If you walk over by the window facing the Bell Tower, you'll find that room to your left.
** 317, 318, 319
If you walk over inside of the innovation space in the right corner, you will find that room along the perimeter.
** 327
You should find it alongside this wall, behind you.
** 328+
Other side of this floor.
* How do I print?
To print, you can do it from any computer or phone that is able to access the file(s) that you want to print.
We have an info card that you can take with you if you need any help, otherwise just google "printing temple university" and you should be able to find a website with all the information you need about printing.
** Step-by-step printing instructions
1. Visit the https://printcenter.temple.edu/ website.
2. Upload the files that you want to print.
3. Configure your printing options (e.g. color or black & white, double-sided or single-sided).
4. Find a printer to print at (the third floor printer is on the other side, in the reading area).
5. Swipe your card at the printer.
6. Select the file(s) that you want to print.
7. Print.
** Note on staplers
They may not have staplers over by the printers.
If this is the case, you can find staplers at the Scholars Studio desk, Student Success Center desk on the 2nd floor, or one stop desk on the first floor.
If there aren't any staples in the staplers, just ask.

I know, we probably should have staplers next to the printers, but maybe whoever decided against it was concerned of theft or something.
* How do I book a study room?
To book a study room, you can visit the library website at https://library.temple.edu/.
There, you'll find a link that says [[https://library.temple.edu/spaces/25]["Book a Study Room."]]
From there, find the study space you're looking for in the list and then find the blue reserve a room button.
You'll be presented with a timetable of the booking status on all of the rooms that you're looking for.
If none of the time slots are green, that means you can't book any rooms for that day.
You'll often notice this is the case if you're booking a room on the same day that you want to use it.
Because of this, I would suggest booking rooms a few days ahead of time, as the rooms get booked real fast.

That being said, there are many students who decide they don't want to use the room anymore, but forget to cancel it on the website.
So, if you happend to find an empty study room, you are welcome to just use it until someone with a booking confirmation shows up.
* Where are the books?
Our space, the Scholars Studio, is not involved in any way with the books.
For that, I would suggest looking online on the library's website at https://library.temple.edu/.
* Is there a quiet place with outlets where I can study?
On the other side of this floor, there is a large study area.
You can find similar spaces on the fourth floor.
* Where is the restroom?
You just passed by it.
If you take a few steps back, you'll find the restroom sign.
Just walk into the hidden space and it'll be on your left.
* What equipment/tech do you have?
All equipment available in our library can be found the same way as you would find a book---online on our website at https://library.temple.edu/.
If it's not showing up, make sure you filter for the "Object" resource type in your search results.

That being said, off the top of my head, I know that we have Nintendo Switches, DSLR cameras, and tripods.
Depending on what you check-out, you will typically have either 4 hours or 1 week to return the item.
* Am I allowed to just sit here/use the desktops?
Yes, you are allowed to sit here.
However, please be aware that the desktop computers are generally reserved for special uses.
You may not sit at the desktops if you are:
1. Just eating food there.
2. Just using the desktops to charge your devices---in that case, you should get a battery pack from the kiosk.
3. Asked to relocate by a staff member.
4. Asked to relocate by a person who needs the desktop for its intended purposes.
If you are just looking for a computer to do simple web-based tasks, please use a laptop which you can get at the laptop kiosk by the staircase.
* What software is available on the desktops?
Specialized software includes, but is not limited to:
- Abbyy Finereader
- Agisoft Metashape
- Anaconda
- Android Studio
- Audacity
- BattleNet
- Blender
- FreeCAD
- Fusion 360
- Gephi
- Gimp
- Google Refine
- Inkscape
- Mallet
- Maya
- Meshmixer
- Origin
- QGIS
- R
- Rstudio
- Steam
- Unity Game Engine
- Unreal Engine
- Visual Studio
If you need a specific software not listed, email [[mailto:digitalscholarship@temple.edu][digitalscholarship@temple.edu]] to ensure availability.
* Where is the VR lab? How do I use it?
You can find the entrance to the VR lab just along this wall.
However, to use the VR lab, you'll need to sign in using the Google Forms on the iPad.
If you're new to the VR lab, you'll need training to get set up with how things work.
Additionally, if you have any belongings on you, you'll need to check-out a locker key, as you are not allowed to bring any of your personal belongings in the room.
If the VR lab happens to be locked, then I can unlock it for you or get one of my colleagues to do so.

Also, if you want, we have wireless VR headsets such as the Oculus Quest and the Oculus Go, if you prefer to use those over the wired ones (Valve Index and Oculus Rift S).

It's hard to keep track of time when you're using VR, but make sure you start wrapping things up when it hits 4:45 PM.
If you're still using it by 4:30 PM, I'll give you a 15 minute warning, so don't worry about it too much.
Just make sure that anything you checked-out gets returned before 5:00 PM, otherwise you will be fined.

Oh, and if you're using it earlier in the day, note that you're only allowed to use the VR set for 4 hours.
If you think you're going to use it for longer than that, then just check in and check out at the desk again.
* Where is the 3D-printing/laser cutting room?
If you walk over to the left corner, you will find the the Makerspace, where they have 3D-printing and laser cutting.
* Where is the Tech Sandbox room?
Over in the innovation space, to your left once you're there.
* You're a student worker, right? How did you get a job here?
Yes, all I did was apply through the [[https://tuportal5.temple.edu/html/TEMPLE/apps/tup/HR/Taleo/index.jsp][Careers@Temple]] link which I found in the "TUAPPLICATIONS" section on TUportal.
* How do you like your job?
My job is fine.
Pretty much all I do right now is sit at the desk and answer general questions.
It's quite relaxed and it allows me to work on schoolwork when I'm not busy.

I'm sure it will be more fun once the technology actually gets all set up.
